<?php
class Gridle
{
    private $transactions;
    private $middle;
    private $end;
    public function findEquilibiaPoint($total,$numbers)
    {
        /*get the middle value to traverse*/
        $middle_value   =   ($total % 2 == 0 ? $total/2 : ($total+1)/2) ;

        /*save the exploded value in a variable so i dont have to explode everytime*/
        if(!isset($this->transactions)){
            /*assuming comma separated values given in the string*/
            $this->transactions  =   array_map('intval', explode(',', $numbers));
        }
        if(!isset($this->middle)){
            /*set the middle value of the transactions*/
            $this->middle   =   $middle_value;
        }
        if(!isset($this->end)){
            /*set the total no of transactions*/
            $this->end   =   $total-1;
        }
        /*get the left and right total of the number around*/
        $left_half     =   array_sum(array_slice($this->transactions,0,$middle_value-1));
        $right_half    =   array_sum(array_slice($this->transactions,$middle_value));

        if($left_half == $right_half){
            echo 'The number is '.$this->transactions[$middle_value-1].' and position is '.($middle_value-1);
        }
        /*logic is get the middle value,compare the left and right sum and go according to it*/
        /*so if right sum is greater than left then i have to traverse left*/
        /*and for traversing i again find the middle value of the stack and compare the right and sum and then again traverse according to it*/
        if($left_half < $right_half){
            if($this->middle    ==  $middle_value || $middle_value<$this->middle){
                $traverse   =   $middle_value % 2 == 0 ?$middle_value/2:($middle_value+1)/2;
            }else{
                $traverse   =   ($middle_value + $this->middle)%2 == 0 ? ($middle_value + $this->middle)/2 :(($middle_value + $this->middle)+1)/2;
            }
            $this->findEquilibiaPoint($traverse*2,$numbers);
        }elseif($left_half > $right_half){
            if($this->middle    ==  $middle_value || $middle_value > $this->middle){
                $traverse   =   ($middle_value + $this->end)%2 == 0 ? ($middle_value + $this->end)/2:(($middle_value + $this->end)+1)/2;
            }else{
                $traverse   =   ($middle_value + $this->middle)%2 == 0 ?($middle_value + $this->middle)/2:(($middle_value + $this->middle)+1)/2;
            }
            $this->findEquilibiaPoint($traverse*2,$numbers);
        }
    }
}
/*
 * '9,0,-5,-4,1,4,-4,-9,0,-7,-1'
 * '9,-7,6,-8,3,-9,-5,3,-6,-8,5'
 * '3,-2,2,0,3,4,-6,3,5,-4,8'
 * '0, -3 ,5, -4, -2 ,3, 1, 0'
 * */
$griddle    =   new Gridle();
$griddle->findEquilibiaPoint(11,'9,0,-5,-4,1,4,-4,-9,0,-7,-1');
